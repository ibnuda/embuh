class Petugas {
  String _nama;
  String _telepon;
  String _password;

  Petugas(this._nama, this._telepon, this._password);

  Petugas.map(dynamic obj) {
    this._nama = obj["nama"];
    this._telepon = obj["telepon"];
    this._password = obj["password"];
  }

  String get nama => _nama;
  String get telepon => _telepon;
  String get password => _password;

  Map<String, dynamic> toMap () {
    var map = new Map<String, dynamic>();
    map["nama"] = _nama;
    map["telepon"] = _telepon;
    map["password"] = _password;
    return map;
  }
}