import 'dart:async';

import 'package:embuh/bantuan/jaringan.dart';
import 'package:embuh/model/petugas.dart';

class LerenRes {
  Jaringan _jaringan = new Jaringan();
  static final ALAMAT_DASAR = "http://sumur.siskam.link";
  static final MLEBU = ALAMAT_DASAR + "/masuk";

  Future<Petugas> login(String telepon, String password) {
    return _jaringan.post(MLEBU, body: {
      "telepon": telepon,
      "password": password
    }).then((dynamic res) {
      print(res.toString());
      if(res["galat"]) throw new Exception(res["galat"]);
      return new Petugas.map(res);
    });
  }
}