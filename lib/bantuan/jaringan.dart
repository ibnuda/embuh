import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

class Jaringan {
  static Jaringan _instans = new Jaringan.internal();
  Jaringan.internal();
  factory Jaringan () => _instans;

  final JsonDecoder _decoder = new JsonDecoder();

  Future<dynamic> get (String url) {
    return http.get(url).then((http.Response resp) {
      final String res = resp.body;
      final int statcod = resp.statusCode;
      if (statcod > 400 || statcod < 200 || json == null) {
        throw new Exception("Galat saat ambil data.");
      }
      return _decoder.convert(res);
    });
  }

  Future<dynamic> post (String url, {Map headers, body}) {
    return http.post(url, body: body, headers: headers).then((http.Response resp) {
      final String res = resp.body;
      final int statcod = resp.statusCode;
      if (statcod > 400 || statcod < 200 || json == null) {
        throw new Exception("Galat saat ambil data.");
      }
      return _decoder.convert(res);
    });
  }
}

